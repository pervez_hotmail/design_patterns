﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns;

namespace DesignPatterns
{
    class ProcessManager
    {
        class SinglteonCreator
        {
            private static readonly ProcessManager instance = new ProcessManager();
            public static ProcessManager ProcessManager
            {
                get { return instance; }
            }
        }
        
        
        static ProcessManager()
        {
            
        }

        public static void Bar()
        {
            
        }
        
        private ProcessManager()
        {
            Console.WriteLine("Hello, World!");   
        }

        private List<Pcb> processes = new List<Pcb>(); 

        public void Fork()
        {
            
        }

        public void Exec()
        {
            
        }

        public static ProcessManager Instance
        {
            get { return SinglteonCreator.ProcessManager; }
        }
    }

    internal class Pcb
    {
        public static bool Foo()
        {
            return true;
        }
    }


}
