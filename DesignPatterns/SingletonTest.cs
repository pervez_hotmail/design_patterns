﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace DesignPatterns
{

    public class SingletonTest
    {
        
        static void Main()
        {
            
            if (Pcb.Foo())
            {
                var p = ProcessManager.Instance;
                p.Fork();
            }
            ProcessManager.Bar();
            Console.WriteLine("Bye");
        }
    }
}
